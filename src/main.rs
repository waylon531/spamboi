extern crate irc;

extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

use irc::client::prelude::*;

use std::io::{Read,BufRead,BufReader};
use std::net::{TcpListener};
use std::thread;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

#[derive(Deserialize)]
struct Message(String,String);

fn main(){
    let panic = Arc::new(AtomicBool::new(false));;
    // Load custom config
    let mut config = Config::load("config.toml").expect("Failed to load config");

    let listener = TcpListener::bind("0.0.0.0:45456").unwrap();
    // Get the stream and throw away the server address
    let mut stream = listener.accept().unwrap().0;
    let mut buf = [0u8; 1];
    stream.read_exact(&mut buf).expect("Unable to read nick");

   
    config.nickname = Some(format!("spamboi{}",buf[0]));

    //let re = Regex::new(r"^!spambot (\w+)").unwrap();
    let mut reactor = IrcReactor::new().unwrap();
    let client = reactor.prepare_client_and_connect(&config).unwrap();
    client.identify().unwrap();

    let cloned_panic = panic.clone();

    reactor.register_client_with_handler(client.clone(), move |_client, _m| {
        if cloned_panic.load(Ordering::SeqCst) == true {
            panic!();
        }
        Ok(())
    });

    thread::spawn(move || {
        let mut stream = BufReader::new(stream);

        // accept connections and process them serially

        let mut data = Vec::new();
        loop {
            
            //Read until end of JSON array
            match stream.read_until(b']', &mut data) {
                //Exit if the master hung up
                Ok(0) => {
                    panic.store(true,Ordering::SeqCst);
                    panic!();
                },
                //Try again on errors
                Err(_) => continue,
                _ => {}
            }


            if let Ok(Message(chan,msg)) = serde_json::from_slice(&data) {
                match client.send(Command::PRIVMSG(chan,msg)) {
                    Ok(_) => {},
                    Err(_) => { //println!("Error sending to IRC server: {:?}",e)
                        panic.store(true,Ordering::SeqCst);
                        panic!();
                    }
                }
                data.clear()
            }
        }
    });

    reactor.run().unwrap();
}
